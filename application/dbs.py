import sqlite3
import os
from flask import g
from application import app

DATABASE = os.path.join(os.path.dirname(__file__), 'dylog_sqlite.db')

def get_account(row):
    return query_db(
        'select * from accounts where vat_number = ?',
        [row['vat_number']],
        single_result=True
    )

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def query_db(query, args=(), single_result=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if single_result else rv