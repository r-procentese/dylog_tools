CONFIG = {
    'type': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0',
    },
    'daybook_number': {
        'required': True,
        'type': 'integer',
        'start_pos': 4,
        'end_pos': 10,
        'alignment': 'right',
        'padding': '0',
    },
    'row_number': {
        'required': True,
        'type': 'integer',
        'start_pos': 11,
        'end_pos': 20,
        'alignment': 'right',
        'padding': '0',
    },
    'series': {
        'required': False,
        'type': 'string',
        'start_pos': 21,
        'end_pos': 23,
        'alignment': 'left',
        'padding': ' ',
    },
    'article': {
        'required': True,
        'type': 'string',
        'start_pos': 24,
        'end_pos': 48,
        'alignment': 'left',
        'padding': ' ',
    },
    'parcel': {
        'required': False,
        'type': 'string',
        'start_pos': 49,
        'end_pos': 63,
        'alignment': 'left',
        'padding': ' ',
    },
    'sw_elab_dist_base': {
        'required': False,
        'type': 'string',
        'start_pos': 64,
        'end_pos': 64,
        'alignment': 'left',
        'padding': ' ',
    },
    'note_type': {
        'required': False,
        'type': 'string',
        'start_pos': 65,
        'end_pos': 67,
        'alignment': 'left',
        'padding': ' ',
    },
    'row_type': {
        'required': True,
        'type': 'string',
        'start_pos': 68,
        'end_pos': 68,
        'alignment': 'left',
        'padding': ' ',
    },
    'row_destination': {
        'required': True,
        'type': 'string',
        'start_pos': 69,
        'end_pos': 69,
        'alignment': 'left',
        'padding': ' ',
    },
    'unit_of_measure': {
        'required': False,
        'type': 'string',
        'start_pos': 70,
        'end_pos': 72,
        'alignment': 'left',
        'padding': ' ',
    },
    'quantity_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 73,
        'end_pos': 73,
        'alignment': 'left',
        'padding': ' ',
    },
    'quantity': {
        'required': True,
        'type': 'integer',
        'start_pos': 74,
        'end_pos': 91,
        'alignment': 'right',
        'padding': '0',
    },
    'net_price_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 92,
        'end_pos': 92,
        'alignment': 'left',
        'padding': ' ',
    },
    'net_price': {
        'required': False,
        'type': 'integer',
        'start_pos': 93,
        'end_pos': 110,
        'alignment': 'right',
        'padding': '0',
    },
    'gross_price_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 111,
        'end_pos': 111,
        'alignment': 'left',
        'padding': ' ',
    },
    'gross_price': {
        'required': False,
        'type': 'integer',
        'start_pos': 112,
        'end_pos': 129,
        'alignment': 'right',
        'padding': '0',
    },
    'flag_vat_included': {
        'required': False,
        'type': 'string',
        'start_pos': 130,
        'end_pos': 130,
        'alignment': 'left',
        'padding': ' ',
    },
    'flag_size_colors': {
        'required': False,
        'type': 'string',
        'start_pos': 131,
        'end_pos': 131,
        'alignment': 'left',
        'padding': ' ',
    },
    'discount_perc_1_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 132,
        'end_pos': 132,
        'alignment': 'left',
        'padding': ' ',
    },
    'discount_perc_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 133,
        'end_pos': 138,
        'alignment': 'right',
        'padding': '0',
    },
    'discount_perc_2_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 139,
        'end_pos': 139,
        'alignment': 'left',
        'padding': ' ',
    },
    'discount_perc_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 140,
        'end_pos': 145,
        'alignment': 'right',
        'padding': '0',
    },
    'discount_perc_3_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 146,
        'end_pos': 146,
        'alignment': 'left',
        'padding': ' ',
    },
    'discount_perc_3': {
        'required': False,
        'type': 'integer',
        'start_pos': 147,
        'end_pos': 152,
        'alignment': 'right',
        'padding': '0',
    },
    'discount_perc_4_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 153,
        'end_pos': 153,
        'alignment': 'left',
        'padding': ' ',
    },
    'discount_perc_4': {
        'required': False,
        'type': 'integer',
        'start_pos': 154,
        'end_pos': 159,
        'alignment': 'right',
        'padding': '0',
    },
    'discount_value_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 160,
        'end_pos': 160,
        'alignment': 'left',
        'padding': ' ',
    },
    'discount_value': {
        'required': False,
        'type': 'integer',
        'start_pos': 161,
        'end_pos': 178,
        'alignment': 'right',
        'padding': '0',
    },
    'amount_type': {
        'required': False,
        'type': 'string',
        'start_pos': 179,
        'end_pos': 179,
        'alignment': 'left',
        'padding': ' ',
    },
    'net_weight_kg': {
        'required': False,
        'type': 'integer',
        'start_pos': 180,
        'end_pos': 189,
        'alignment': 'right',
        'padding': '0',
    },
    'gross_weight_kg': {
        'required': False,
        'type': 'integer',
        'start_pos': 190,
        'end_pos': 199,
        'alignment': 'right',
        'padding': '0',
    },
    'vat_code': {
        'required': True,
        'type': 'string',
        'start_pos': 200,
        'end_pos': 202,
        'alignment': 'left',
        'padding': ' ',
    },
    'agent_commission_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 203,
        'end_pos': 208,
        'alignment': 'right',
        'padding': '0',
    },
    'agent_commission_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 209,
        'end_pos': 214,
        'alignment': 'right',
        'padding': '0',
    },
    'agent_commission_ca': {
        'required': False,
        'type': 'integer',
        'start_pos': 215,
        'end_pos': 220,
        'alignment': 'right',
        'padding': '0',
    },
    'commodity_category': {
        'required': False,
        'type': 'string',
        'start_pos': 221,
        'end_pos': 224,
        'alignment': 'left',
        'padding': ' ',
    },
    'article_category': {
        'required': True,
        'type': 'string',
        'start_pos': 225,
        'end_pos': 227,
        'alignment': 'left',
        'padding': ' ',
    },
    'homogeneous_category': {
        'required': False,
        'type': 'string',
        'start_pos': 228,
        'end_pos': 231,
        'alignment': 'left',
        'padding': ' ',
    },
    'master_account_revenue_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 232,
        'end_pos': 234,
        'alignment': 'right',
        'padding': '0',
    },
    'main_account_revenue_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 235,
        'end_pos': 236,
        'alignment': 'right',
        'padding': '0',
    },
    'account_revenue_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 237,
        'end_pos': 238,
        'alignment': 'right',
        'padding': '0',
    },
    'subaccount_revenue_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 239,
        'end_pos': 243,
        'alignment': 'right',
        'padding': '0',
    },
    'main_account_accrued_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 244,
        'end_pos': 246,
        'alignment': 'right',
        'padding': '0',
    },
    'accrued_type_1': {
        'required': False,
        'type': 'string',
        'start_pos': 247,
        'end_pos': 247,
        'alignment': 'left',
        'padding': ' ',
    },
    'account_accrued_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 248,
        'end_pos': 249,
        'alignment': 'right',
        'padding': '0',
    },
    'subaccount_accrued_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 250,
        'end_pos': 254,
        'alignment': 'right',
        'padding': '0',
    },
    'competence_sign_1': {
        'required': False,
        'type': 'string',
        'start_pos': 255,
        'end_pos': 255,
        'alignment': 'left',
        'padding': ' ',
    },
    'competence_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 256,
        'end_pos': 261,
        'alignment': 'right',
        'padding': '0',
    },
    'main_account_accrued_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 262,
        'end_pos': 264,
        'alignment': 'right',
        'padding': '0',
    },
    'accrued_type_2': {
        'required': False,
        'type': 'string',
        'start_pos': 265,
        'end_pos': 265,
        'alignment': 'left',
        'padding': ' ',
    },
    'account_accrued_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 266,
        'end_pos': 267,
        'alignment': 'right',
        'padding': '0',
    },
    'subaccount_accrued_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 268,
        'end_pos': 272,
        'alignment': 'right',
        'padding': '0',
    },
    'competence_sign_2': {
        'required': False,
        'type': 'string',
        'start_pos': 273,
        'end_pos': 273,
        'alignment': 'left',
        'padding': ' ',
    },
    'competence_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 274,
        'end_pos': 279,
        'alignment': 'right',
        'padding': '0',
    },
    'deposit_code': {
        'required': True,
        'type': 'string',
        'start_pos': 280,
        'end_pos': 282,
        'alignment': 'left',
        'padding': ' ',
    },
    'destination_deposit_code': {
        'required': False,
        'type': 'string',
        'start_pos': 283,
        'end_pos': 285,
        'alignment': 'left',
        'padding': ' ',
    },
    'vs_article': {
        'required': False,
        'type': 'string',
        'start_pos': 286,
        'end_pos': 310,
        'alignment': 'left',
        'padding': ' ',
    },
    'list_code': {
        'required': True,
        'type': 'string',
        'start_pos': 311,
        'end_pos': 315,
        'alignment': 'left',
        'padding': ' ',
    },
    'quantity_acquired': {
        'required': False,
        'type': 'integer',
        'start_pos': 316,
        'end_pos': 317,
        'alignment': 'right',
        'padding': '0',
    },
    'quantity_paid': {
        'required': False,
        'type': 'integer',
        'start_pos': 318,
        'end_pos': 319,
        'alignment': 'right',
        'padding': '0',
    },
    'filler': {
        'required': False,
        'type': 'integer',
        'start_pos': 320,
        'end_pos': 321,
        'alignment': 'right',
        'padding': '0',
    },
    'intra_natura': {
        'required': False,
        'type': 'integer',
        'start_pos': 322,
        'end_pos': 322,
        'alignment': 'right',
        'padding': '0',
    },
    'intra_stat_value_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 323,
        'end_pos': 323,
        'alignment': 'left',
        'padding': ' ',
    },
    'intra_stat_value': {
        'required': False,
        'type': 'integer',
        'start_pos': 324,
        'end_pos': 341,
        'alignment': 'right',
        'padding': '0',
    },
    'intra_regime': {
        'required': False,
        'type': 'integer',
        'start_pos': 342,
        'end_pos': 344,
        'alignment': 'right',
        'padding': '0',
    },
    'intra_transport_module': {
        'required': False,
        'type': 'integer',
        'start_pos': 345,
        'end_pos': 347,
        'alignment': 'right',
        'padding': '0',
    },
    'intra_country_of_destination': {
        'required': False,
        'type': 'string',
        'start_pos': 348,
        'end_pos': 349,
        'alignment': 'left',
        'padding': ' ',
    },
    'intra_country_of_origin': {
        'required': False,
        'type': 'string',
        'start_pos': 350,
        'end_pos': 351,
        'alignment': 'left',
        'padding': ' ',
    },
    'package_number': {
        'required': False,
        'type': 'integer',
        'start_pos': 352,
        'end_pos': 369,
        'alignment': 'right',
        'padding': '0',
    },
    'broken_package_number': {
        'required': False,
        'type': 'integer',
        'start_pos': 370,
        'end_pos': 387,
        'alignment': 'right',
        'padding': '0',
    },
    'commission': {
        'required': False,
        'type': 'string',
        'start_pos': 388,
        'end_pos': 394,
        'alignment': 'left',
        'padding': ' ',
    },
    'barcode': {
        'required': False,
        'type': 'string',
        'start_pos': 395,
        'end_pos': 419,
        'alignment': 'left',
        'padding': ' ',
    },
    'sw_code': {
        'required': False,
        'type': 'string',
        'start_pos': 420,
        'end_pos': 420,
        'alignment': 'left',
        'padding': ' ',
    },
    'competence_perc_sign_1': {
        'required': False,
        'type': 'string',
        'start_pos': 421,
        'end_pos': 421,
        'alignment': 'left',
        'padding': ' ',
    },
    'competence_perc_1': {
        'required': False,
        'type': 'integer',
        'start_pos': 422,
        'end_pos': 428,
        'alignment': 'right',
        'padding': '0',
    },
    'competence_perc_sign_2': {
        'required': False,
        'type': 'string',
        'start_pos': 429,
        'end_pos': 429,
        'alignment': 'left',
        'padding': ' ',
    },
    'competence_perc_2': {
        'required': False,
        'type': 'integer',
        'start_pos': 430,
        'end_pos': 436,
        'alignment': 'right',
        'padding': '0',
    },
    'list_unit_of_measure': {
        'required': False,
        'type': 'string',
        'start_pos': 437,
        'end_pos': 439,
        'alignment': 'left',
        'padding': ' ',
    },
    'list_net_price_sign': {
        'required': False,
        'type': 'string',
        'start_pos': 440,
        'end_pos': 440,
        'alignment': 'left',
        'padding': ' ',
    },
    'list_net_price': {
        'required': False,
        'type': 'integer',
        'start_pos': 441,
        'end_pos': 458,
        'alignment': 'right',
        'padding': '0',
    },
    'description_memo': {
        'required': False,
        'type': 'string',
        'start_pos': 459,
        'end_pos': 559,
        'alignment': 'left',
        'padding': ' ',
    },
}

DEFAULTS = {
    'type': 340,
    'daybook_number': 0,
    'row_number': 0,
    'article': '',
    'row_type': '',
    'row_destination': '',
    'quantity': 0,
    'vat_code': '',
    'article_category': '',
    'deposit_code': '',
    'list_code': '',
    'quantity_sign': '+',
    'net_price_sign': '+',
    'gross_price_sign': '+',
    'discount_perc_1_sign': '+',
    'discount_perc_2_sign': '+',
    'discount_perc_3_sign': '+',
    'discount_perc_4_sign': '+',
    'discount_value_sign': '+',
    'competence_sign_1': '+',
    'competence_sign_2': '+',
    'intra_stat_value_sign': '+',
    'competence_perc_sign_1': '+',
    'competence_perc_sign_2': '+',
    'list_net_price_sign': '+',
}
