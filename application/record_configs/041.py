CONFIG = {
    '041.01': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0',
    },
    '041.02': {
        'required': False,
        'type': 'integer',
        'start_pos': 4,
        'end_pos': 6,
        'alignment': 'right',
        'padding': '0',
    },
    '041.03': {
        'required': False,
        'type': 'integer',
        'start_pos': 7,
        'end_pos': 8,
        'alignment': 'right',
        'padding': '0',
    },
    '041.04': {
        'required': False,
        'type': 'integer',
        'start_pos': 9,
        'end_pos': 10,
        'alignment': 'right',
        'padding': '0',
    },
    '041.05': {
        'required': False,
        'type': 'integer',
        'start_pos': 11,
        'end_pos': 15,
        'alignment': 'right',
        'padding': '0',
    },
    '041.06': {
        'required': False,
        'type': 'string',
        'start_pos': 16,
        'end_pos': 16,
        'alignment': 'left',
        'padding': ' ',
    },
    '041.07': {
        'required': False,
        'type': 'string',
        'start_pos': 17,
        'end_pos': 17,
        'alignment': 'left',
        'padding': ' ',
    },
    '041.08': {
        'required': False,
        'type': 'string',
        'start_pos': 18,
        'end_pos': 45,
        'alignment': 'left',
        'padding': ' ',
    },
    '041.09': {
        'required': False,
        'type': 'string',
        'start_pos': 46,
        'end_pos': 46,
        'alignment': 'left',
        'padding': ' ',
    },
    '041.10': {
        'required': False,
        'type': 'string',
        'start_pos': 47,
        'end_pos': 47,
        'alignment': 'left',
        'padding': ' ',
    },
    '041.11': {
        'required': False,
        'type': 'string',
        'start_pos': 48,
        'end_pos': 48,
        'alignment': 'left',
        'padding': ' ',
    },
    '041.12': {
        'required': False,
        'type': 'string',
        'start_pos': 49,
        'end_pos': 78,
        'alignment': 'left',
        'padding': ' ',
    },
}

DEFAULTS = {
    '041.01': 0,
}
