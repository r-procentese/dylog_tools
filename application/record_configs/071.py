CONFIG = {
    'record_type': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0',
    },
    'codice_anagrafico': {
        'required': True,
        'type': 'integer',
        'start_pos': 4,
        'end_pos': 10,
        'alignment': 'right',
        'padding': '0',
    },
    'vat_number': {
        'required': False,
        'type': 'string',
        'start_pos': 11,
        'end_pos': 22,
        'alignment': 'left',
        'padding': ' ',
    },
    'fiscal_code': {
        'required': False,
        'type': 'string',
        'start_pos': 23,
        'end_pos': 38,
        'alignment': 'left',
        'padding': ' ',
    },
    'percentage_split_cointestatario': {
        'required': True,
        'type': 'integer',
        'start_pos': 39,
        'end_pos': 45,
        'alignment': 'right',
        'padding': '0',
    },
}

DEFAULTS = {
    '071.01': 0,
    '071.02': 0,
    '071.04': 0,
}
