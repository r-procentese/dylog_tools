CONFIG = {
    '215.01': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0',
    },
    '215.02': {
        'required': True,
        'type': 'string',
        'start_pos': 4,
        'end_pos': 6,
        'alignment': 'left',
        'padding': ' ',
    },
    '215.03': {
        'required': False,
        'type': 'string',
        'start_pos': 7,
        'end_pos': 1005,
        'alignment': 'left',
        'padding': ' ',
    },
}

DEFAULTS = {
    '215.01': 0,
    '215.02': '',
}
