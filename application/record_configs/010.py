from datetime import datetime

CONFIG = {
    'type': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0',
    },
    'company_code': {
        'required': False,
        'type': 'integer',
        'start_pos': 4,
        'end_pos': 7,
        'alignment': 'left',
        'padding': '0',
    },
    'vat_code': {
        'required': True,
        'type': 'string',
        'start_pos': 8,
        'end_pos': 19,
        'alignment': 'left',
        'padding': ' ',
    },
    'end_accounting_month': {
        'required': True,
        'type': 'integer',
        'start_pos': 20,
        'end_pos': 21,
        'alignment': 'left',
        'padding': '0',
    },
    'end_accounting_year': {
        'required': True,
        'type': 'integer',
        'start_pos': 22,
        'end_pos': 25,
        'alignment': 'left',
        'padding': '0',
    },
    'accounting_type': {
        'required': False,
        'type': 'string',
        'start_pos': 26,
        'end_pos': 26,
        'alignment': 'left',
        'padding': ' ',
    },
    'accounting_level': {
        'required': False,
        'type': 'integer',
        'start_pos': 27,
        'end_pos': 27,
        'alignment': 'left',
        'padding': '0',
    },
}

DEFAULTS = {
    'type': 10,
    'company_code': 1,
    'vat_code': '09294100962',
    'end_accounting_month': 12,
    'accounting_type': 'O',
    'end_accounting_year': datetime.now().year,
}
