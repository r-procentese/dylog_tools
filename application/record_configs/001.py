CONFIG = {
    'type': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0'
    },
    'identifier': {
        'required': True,
        'type': 'string',
        'start_pos': 4,
        'end_pos': 39,
        'alignment': 'left',
        'padding': ' '
    },
    'release': {
        'required': True,
        'type': 'string',
        'start_pos': 40,
        'end_pos': 47,
        'alignment': 'left',
        'padding': ' '
    },
    'description': {
        'required': False,
        'type': 'string',
        'start_pos': 48,
        'end_pos': 97,
        'alignment': 'left',
        'padding': ' '
    },
    'creation_date': {
        'required': False,
        'type': 'string',
        'start_pos': 98,
        'end_pos': 107,
        'alignment': 'left',
        'padding': ' '
    },
}

DEFAULTS = {
    'type': 1,
    'identifier': 'DylogTracciatoRecordImportazioneRel.',
    'release': '01.16.00'
}
