CONFIG = {
    '052.01': {
        'required': True,
        'type': 'integer',
        'start_pos': 1,
        'end_pos': 3,
        'alignment': 'right',
        'padding': '0',
    },
    '052.02': {
        'required': True,
        'type': 'integer',
        'start_pos': 4,
        'end_pos': 10,
        'alignment': 'right',
        'padding': '0',
    },
    '052.29': {
        'required': False,
        'type': 'string',
        'start_pos': 531,
        'end_pos': 630,
        'alignment': 'left',
        'padding': ' ',
    },
    '052.30': {
        'required': False,
        'type': 'string',
        'start_pos': 631,
        'end_pos': 730,
        'alignment': 'left',
        'padding': ' ',
    },
    '052.31': {
        'required': False,
        'type': 'string',
        'start_pos': 731,
        'end_pos': 830,
        'alignment': 'left',
        'padding': ' ',
    },
}

DEFAULTS = {
    '052.01': 0,
    '052.02': 0,
}
