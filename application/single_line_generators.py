import csv
from datetime import datetime
from dateutil import parser
from copy import deepcopy
from importlib import import_module
from fixedwidth.fixedwidth import FixedWidth

from application.services import *
from application import app

def generate(code, user_dict=None):
    record = load_module(code)
    base_dict = deepcopy(record.DEFAULTS)

    if user_dict:
        base_dict.update(user_dict)

    export = FixedWidth(record.CONFIG)
    export.update(**base_dict)
    return export.line

def generate_050():
    """Returns a fully ready Tracciato for Anagrafiche"""
    output = ""
    with open('anagrafiche.csv', newline='') as csvfile:
        reader = csv.DictReader(
            csvfile,
            fieldnames=[
                'business_name',
                'vat_number',
                'registered_office_address',
                'registered_office_zip'
                ],
            delimiter=';'
            )

        for row in reader:
            user_dict = {
                'rag_sociale_1': row['business_name'],
                'vat_number': row['vat_number'],
                'legal_address_street': row['registered_office_address'],
                'legal_address_cap': row['registered_office_zip'],
            }
            output += generate('050', user_dict)

    return output


def generate_040(account):

    if not account:
        return ""

    user_dict = {
        'master_account': account['master_account'],
        'main_account': account['main_account'],
        'account': account['account'],
        'subaccount': account['subaccount'],
    }

    return generate('040', user_dict)

def generate_073(row, conti, dare_avere):
    master_account, account, subaccount = conti.split(".")

    user_dict = {
        'master_account': int(master_account),
        'subaccount': int(subaccount),
        'account': int(account),
        'amount': abs(int(row.importo * 10000)),
        'amount_sign': "+",# if int(row.importo) >= 0 else "-",
        'switch_debit_credit': dare_avere[:1].upper(),
    }
    return generate('073', user_dict=user_dict)

def generate_330(row, account):
    ddt_number = row['ddt_code']
    ddt_date = parser.parse(row['ddt_created'])
    billed_date = None
    bill_number = None
    if row.get('invoice_date', None) and row.get('invoice_number', None):
        billed_date = parser.parse(row['invoice_date'])
        bill_number = int(row['invoice_number'].split('/')[0])

    if ddt_number.startswith('P'):
        ddt_number = ddt_number[1:]
    ddt_number = int(ddt_number)

    user_dict = {
        'daybook_number': 0,
        'daybook_date': ddt_date.strftime('%d/%m/%Y'),
        'section': 1,
        'client_master_account': account['master_account'],
        'client_main_account': account['main_account'],
        'client_account': account['account'],
        'client_subaccount': account['subaccount'],
        'change': 0,
        'stock_reason': '001',
        'sw_print_documents': '5',
        'sw_installment_type': '0',
        'ddt_date': ddt_date.strftime('%d/%m/%Y'),
        'date_of_effect': ddt_date.strftime('%d/%m/%Y'),
        'ddt_type': 'D',
        'ddt_number': ddt_number,
        'subsidiary_id': 1,
        'sw_issued_bill': '0',
        'list_code': '00000',
        'deposit_code': '000',
        'deposit_destination': '000',
        'currency_code': 'E',
        'accounting_reason': '001',
        'transported_by': 'V',
        'transport_reason': 'Vendita',
    }

    if bill_number and billed_date:
        user_dict.update({
            'bill_type': 'F',
            'bill_number': bill_number,
            'billed_date': billed_date.strftime('%d/%m/%Y'),
        })
    
    return generate('330', user_dict)

def generate_340(row, row_n):
    is_hg = False
    if row.get('unit_definition', None):
        if row.get('sale_unit_measure_approx', None) == 't':
            is_hg = int(row['sale_unit_measure']) == 3
            unit_of_measure = get_sale_unit_measure(int(row['sale_unit_measure']))
        else:
            is_hg = int(row['unit_definition']) == 8
            unit_of_measure = get_unit_type_description(int(row['unit_definition']), row['unit_of_measure'])
    else:
        unit_of_measure = row['unit_of_measure']

    quantity = float(row['confirmed_um_quantity'])
    if is_hg:
        quantity *= 10

    net_price = int(get_unit_price_confirmed(float(row['total_price_confirmed']), quantity) * 10000)
    vat_code = str(row['vat_code']).zfill(3)
                
    user_dict = {
        'daybook_number': 0,
        'row_number': row_n+1,
        'article': '%s' % str(row['article_code']),
        'description_memo': str(row['description']),
        'row_type': '1',
        'row_destination': '1',
        'unit_of_measure': unit_of_measure,
        'quantity': int(quantity * 10000),
        'net_price': net_price,
        'vat_code': vat_code,
        'article_category': '001',
        'deposit_code': '000',
        'list_code': '00000',
    }
                
    return generate('340', user_dict)