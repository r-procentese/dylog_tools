from importlib import import_module

def load_module(code):
    return import_module(
        'application.record_configs.%s' % code,
        package='application.record_configs'
        )

def get_sale_unit_measure(unit_measure):
    LIST = (
        (0, ''),
        (1, 'Kg'),
        (2, 'Lt'),
        (3, 'Kg')
    )

    unit = LIST[unit_measure][1]
    return "%s" % unit

def get_unit_type_description(unit_definition, sale_unit_type_other):
    LIST = (
        (0, ''),
        (1, 'N'),
        (2, 'N'),
        (3, 'N'),
        (4, 'N'),
        (5, 'Kg'),
        (6, 'Lt'),
        (7, 'N'),
        (8, 'Kg'),
        (9, 'Pz'),
    )

    unit = ""
    if unit_definition > 0:
        if unit_definition == 7:
            unit = 'N'
            # unit = sale_unit_type_other
        else:
            unit = LIST[unit_definition][1]
    return "%s" % unit

def get_unit_price_confirmed(total_price_confirmed, confirmed_um_quantity):
    try:
        get_unit_price = total_price_confirmed / confirmed_um_quantity
        return round(get_unit_price, 2)
    except:
        get_unit_price = 0
        return get_unit_price


def sanitize(s):
    swaps = {'à': 'a\'',
        'è': 'e\'',
        'é': 'e\'',
        'ò': 'o\'',
        'ù': 'u\'',
        'ì': 'i\'',
        '€': 'eur',
        '$': 'usd'
    }
    for swap in swaps.keys():
        s = s.replace(swap, swaps[swap])
    all_ascii = ''.join(char for char in s if ord(char) < 128)
    return s