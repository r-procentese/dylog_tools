# Export for Marketplace

## RUN ON DYLOG SERVER

### Import DDT on Dylog
```
"C:\Program Files (x86)\OpenManager\A00807.EXE" Dyopeng "C:\Program Files (x86)\OpenManager\" BATCH MVS "%userprofile%\Desktop\export_tracciato.txt" '' DCF_S PRINT_S CFO_N IPD_N NO_MOV_N NUM_BRO_N BRO_IMP_S 1 EXS_N EXA_N TRD_N FC_N 1 F X B D 0003 AGA_N NORM_S
```

## DATA NEEDED ON THIS TOOL

### Export CSV: DDTS MARKETPLACE
```
\copy (select u.vat_number, p.code, left(t.name, 50), p.unit_definition, t.sale_unit_type_other, p.sale_unit_measure, p.sale_unit_measure_approx, oi.confirmed_um_quantity, oi.total_price_confirmed, p.vat, ddt.id, ddt.ddt_code, DATE(ddt.created), inv.number, inv.invoice_date from shop_product p join shop_product_translation t on p.id = t.master_id join shop_orderitem oi on oi.product_id = p.id join shop_order o on o.id = oi.order_id join ddt_ddt ddt on ddt.order_id = o.id join invoice_invoice inv on inv.id = o.invoice_id join users_user u on u.id = o.user_id where o.ddt_confirmed is True order by ddt.id asc) to '/code/ddts.csv' delimiter ';' quote E'\b' CSV
```

### Export CSV: DDTS DaaS
```
\copy (select u.vat_number, p.code, left(p.name, 50), null, null, null, p.unit_of_measure, oi.confirmed_um_quantity, oi.total_price_confirmed, p.vat, ddt.id, ddt.ddt_code, DATE(ddt.created), inv.number, inv.invoice_date from shop_product p join shop_orderitem oi on oi.product_id = p.id join shop_order o on o.id = oi.order_id join ddt_ddt ddt on ddt.order_id = o.id left join invoice_invoice inv on inv.id = o.invoice_id join users_user u on u.id = o.user_id where o.ddt_confirmed is True order by ddt.id asc) to '/app/ddts.csv' delimiter ';' quote E'\b' CSV
```

## DATA NEEDED TO GENERATE dylog_sqlite.db

### Export CSV: Utenti
```
\copy (select u.business_name, u.vat_number, u.registered_office_address, u.registered_office_zip from users_user u) to '/code/anagrafiche.csv' delimiter ';' quote E'\b' CSV
```

## IMPORTAZIONE PARAMETRICA DYLOG

### Export CSV: Prodotti MARKETPLACE
```
\copy (select p.code, left(t.name, 50), '0', 'KG' from shop_product p join shop_product_translation t on p.id = t.master_id order by p.id) to '/home/direttoo/products.csv' delimiter ';' quote E'\b' CSV;
```

### Export CSV: Prodotti DaaS
```
\copy (select code, left(name, 50), '0', unit_of_measure from shop_product p order by p.id) to '/home/direttoo/products.csv' delimiter ';' quote E'\b' CSV;
```