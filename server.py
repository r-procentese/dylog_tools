import os
import sys

from flask import g, request

import csv
import datetime
import json as json_og
from pandas import *
from application import app
from application.dbs import get_account
from application.generate_record import RECORD_FIELDS
from application.single_line_generators import *
from importlib import import_module


CODES = [
    '001',
    '010',
    '040',
    '050',
    '300',
    '310',
    '320',
    '330',
    '331',
    '340',
]
ddt_fieldnames = [
    'vat_number',
    'article_code',
    'description',
    'unit_definition',
    'unit_of_measure',
    'sale_unit_measure',
    'sale_unit_measure_approx',
    'confirmed_um_quantity',
    'total_price_confirmed',
    'vat_code',
    'ddt_id',
    'ddt_code',
    'ddt_created',
    'invoice_number',
    'invoice_date'
]



@app.route('/generate_records', methods=["GET"])
def generate_records():
    for record_field in RECORD_FIELDS:
        output = ''
        params_output = ''
        file_name = 'application/record_configs/%s.py' % record_field
        output += 'CONFIG = {\n'
        params_output += 'DEFAULTS = {\n'
        file_path = os.path.join(os.path.dirname(__file__), file_name)

        if os.path.isfile(file_path):
            continue

        with open(file_path, 'w') as out:
            for record in RECORD_FIELDS[record_field]:
                if record.pos_end:
                    output += "    '%s': {\n" % record.field_id
                    output += "        'required': %s,\n" % (True if record.mandatory == 'S' else False)
                    output += "        'type': '%s',\n" % ('string' if record.type.startswith('A') else 'integer')
                    output += "        'start_pos': %d,\n" % record.pos_start
                    output += "        'end_pos': %d,\n" % record.pos_end
                    output += "        'alignment': '%s',\n" % ('left' if record.type.startswith('A') else 'right')
                    output += "        'padding': '%s',\n" % (' ' if record.type.startswith('A') else '0')
                    output += '    },\n'

                    if record.mandatory == 'S':
                        params_output += "    '{0}': {1},\n".format(record.field_id, "''" if record.type.startswith('A') else 0)
            params_output += '}\n'
            output += '}\n\n'

            out.write(output)
            out.write(params_output)
    return 'OK\n'

@app.route('/generate_movcont', methods=["GET"])
def generate_movconts():
    output = ''

    output += generate('001')
    output += generate('010')


    xls = ExcelFile('data/movcont.xlsx')
    df = xls.parse(xls.sheet_names[0], converters={'conti1': str, 'conti2': str, 'data': str, 'codice_contabile': str})
    pandas.set_option('display.max_colwidth', -1)

    now = datetime.now()
    thisyear = now.year


    for row in df.itertuples():
        user_dict_070 = {
            'registration_date': row.data[:10],
            'document_date' : row.data[:10],
            'esercizio_di_competenza': 'C' if thisyear==row.es_competenza else 'P',
            'codice_causale_contabile': row.codice_contabile,
            'movement_description': sanitize(row.descrizione1)[:50],
            'total_amount': abs(int(row.importo * 10000)),
            'total_amount_sign': "+",# if int(row.importo) >= 0 else "-",
            'currency_code': row.valuta[:1],
            'data_incasso_pagamento': "00:00:00"
        }

        output += generate('070', user_dict=user_dict_070)

        output += generate_073(row, row.conti1, row.dare_avere1)
        output += generate_073(row, row.conti2, row.dare_avere2)


    with open(os.path.join(os.path.dirname(__file__), 'export_movcont.txt'), 'w', newline='') as out:
        out.write(output)
    return output


@app.route('/generate_ddts', methods=["GET", "POST"])
def generate_ddts():
    output = generate_ddt_headers()
    if request.method=='GET':
        output += csv_to_ddt()
    if request.method=='POST':
        output += json_to_ddt(request.json)

    with open(os.path.join(os.path.dirname(__file__), 'export_tracciato.txt'), 'w', newline='') as out:
        out.write(output)
    import_latest_ddt()
    return output

@app.route('/import_latest_ddt', methods=["GET"])
def import_latest_ddt():
    fname = os.path.join(os.path.dirname(__file__), 'export_tracciato.txt')
    import subprocess
    subprocess.run("C:\\dylog_tools\\import.bat")
    return "WE DON'T KNOW. WE CAN'T KNOW THE STATUS OF THIS. STATUS.CODE=???"

def csv_to_ddt():
    output = ""
    with open('ddts.csv', newline='') as csvfile:
        reader = csv.DictReader(
            csvfile,
            fieldnames=ddt_fieldnames,
            delimiter=';'
            )


        ddt_id = None
        for row_n, row in enumerate(reader):
            account = get_account(row)

            if row['ddt_id'] != ddt_id:
                ddt_id = row['ddt_id']
                output += generate_330(row, account)
                output += generate('331')        

            output += generate_340(row, row_n)
    return output

def json_to_ddt(data):
    account = get_account(data)
    output = ""
    output += generate_330(data, account)
    output += generate('331')
    row_n = 1
    for order_item in data['order_items']:
        output += generate_340(order_item, row_n)
        row_n += 1
    return output
    

def generate_ddt_headers():
    output = ''
    record = {}
    for code in CODES:
        record[code] = import_module(
            'application.record_configs.%s' % code,
            package='application.record_configs'
            )

    output += generate('001')
    output += generate('010')
    #output += generate_050()
    return output
